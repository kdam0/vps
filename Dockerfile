FROM python:3-slim-bullseye

RUN apt-get update -y; \
    apt-get install -y git make sshpass;

RUN adduser -u 1000 ansible
USER ansible
WORKDIR /home/ansible/vps
COPY . .
RUN make install;
