# ANSIBLE-MAKEFILE

SHELL:=/usr/bin/env bash

##
# VARIABLES
##
playbook         ?= setup
roles_path       ?= "roles/"
env_path         ?= "inventories/"
group            ?= all
venv_path  	 := "${HOME}/.venv/vps/"

mkfile_dir       ?= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
ifeq ("$(wildcard $(mkfile_dir)/vaultid)", "")
	opts     ?= $(args)
else # Handle vault password if any
	opts     ?= $(args) --vault-password-file=$(mkfile_dir)/vaultid
endif

ifneq ("$(limit)", "")
	opts     := $(opts) --limit="$(limit)"
endif

ifneq ("$(tags)", "")
	opts     := $(opts) --tags="$(tags)"
endif

ifneq ("$(skip)", "")
	opts     := $(opts) --skip-tags="$(skip)"
endif

##
# TASKS
##
.PHONY: install
install: ## make install # Install ansible, ansible-lint, and required collections. Reveal vaultid file.
	@test -d ${venv_path} || (python3 -m venv ${venv_path} --system-site-packages)
	@source ${venv_path}/bin/activate && pip install -r requirements.txt
	@source ${venv_path}/bin/activate && ansible-galaxy install -r requirements.yml
	@source ${venv_path}/bin/activate && ansible-galaxy collection install --ignore-errors -r requirements.yml
	@[ -f "vaultid" ] && source ${venv_path}/bin/activate && ansible-vault decrypt --vault-id vaultid --output hosts hosts.vault || echo "Nothing to decrypt with. Bye."

.PHONY: lint
lint: mandatory-file-param ## make lint file=playbooks/template_playbook.yaml # Run ansible-lint on a playbook or tasks file.
	@[ -f "$(file)" ] && ANSIBLE_VAULT_PASSWORD_FILE=vaultid ansible-lint -c ansible-lint.yaml "$(file)"

.PHONY: syntax
syntax: mandatory-file-param ## make syntax file=playbooks/template_playbook.yaml [args=<ansible-playbook arguments>] # Check syntax of a playbook.
	@[ -f "$(file)" ] && ansible-playbook --inventory-file="hosts" --syntax-check $(opts) "$(file)"

.PHONY: dry-run
dry-run: mandatory-file-param ## make dry-run file=playbooks/template_playbook.yaml [tags=<ansible tags>] [limit=<ansible host limit>] [args=<ansible-playbook arguments>] # Run a playbook in dry run mode.
	@[ -f "$(file)" ] && source ${venv_path}/bin/activate && ansible-playbook --inventory-file="hosts" --diff --check $(opts) "$(file)"

.PHONY: run
run: mandatory-file-param ## make run file=playbooks/template_playbook.yaml [tags=<ansible tags>] [limit=<ansible host limit>] [args=<ansible-playbook arguments>] # Run a playbook.
	@[ -f "$(file)" ] && source ${venv_path}/bin/activate && ansible-playbook --inventory-file="hosts" --diff $(opts) "$(file)"

.PHONY: vault-view
vault-view: ## make vault-view [group=all] [args=<ansible-vault arguments>] # View a vaulted file.
	@[ -f "group_vars/${group}/vault" ] && source ${venv_path}/bin/activate  && ansible-vault view $(opts) "group_vars/${group}/vault"

.PHONY: vault-edit
vault-edit: ## make vault-edit [group=all] [args=<ansible-vault arguments>] # Edit a vaulted file.
	@[ -f "group_vars/${group}/vault" ] && source ${venv_path}/bin/activate  && ansible-vault edit $(opts) "group_vars/${group}/vault"

.PHONY: facts
facts: ## make facts [group=all] [args=<ansible arguments>] # Gather facts from your hosts.
	@source ${venv_path}/bin/activate && ansible --module-name="setup" --inventory-file="hosts" $(opts) --tree="out/" $(group)

.PHONY: mandatory-host-param mandatory-file-param
mandatory-host-param:
	@[ ! -z $(host) ]
mandatory-file-param:
	@[ ! -z $(file) ]

.PHONY: help
help: ## make help # Print this help message.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
