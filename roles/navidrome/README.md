# Ansible - Navidrome

## Example Playbook
```
# Access: http://art-jr:4533/music/
# Domain Access: http://kumardamani.xyz/music
# Create login from GUI
- hosts: compute
  gather_facts: false
  tags: 'navidrome'
  become: true
  roles:
    - role: navidrome
      vars:
        navidrome_domain: 'kumardamani.xyz'
        navidrome_data_root: '{{ data_vol_ssd }}/music'
```

## Nginx Conf
```
            location /music/ {
                proxy_pass http://127.0.0.1:4533/music/;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Protocol $scheme;
                proxy_set_header X-Forwarded-Host $http_host;
                proxy_buffering off;
            }
```
