# Ansible - Seafile

## Example Playbook
```
# Direct Access: http://art-jr:8000/
# Domain Access: http://kumardamani.xyz/files
- hosts: compute
  gather_facts: false
  tags: 'seafile'
  become: true
  roles:
    - role: seafile_non_docker
      vars:
        seafile_user_email: 'kumar@kumardamani.xyz'
        seafile_user_password: '{{ vault_seafile_user_password }}'
        seafile_domain: 'kumardamani.xyz'
        seafile_data_dir: '{{ data_vol_hdd }}/seafile'
```

## Nginx Conf
```
            location /files/ {
                 proxy_pass         http://127.0.0.1:8000;
                 proxy_set_header   Host $host;
                 proxy_set_header   X-Real-IP $remote_addr;
                 proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
                 proxy_set_header   X-Forwarded-Host $server_name;
                 proxy_set_header   X-Forwarded-Proto https;
                 proxy_read_timeout  1200s;
                 # used for view/edit office file via Office Online Server
                 client_max_body_size 0;
                 error_log       /var/log/nginx/seahub.error.log;
            }
            location /seafhttp/ {
                rewrite ^/seafhttp(.*)$ $1 break;
                proxy_pass http://127.0.0.1:8082;
                client_max_body_size 0;
                proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_connect_timeout  36000s;
                proxy_read_timeout  36000s;
                proxy_send_timeout  36000s;
                send_timeout  36000s;
                error_log       /var/log/nginx/seafhttp.error.log;
            }
            location /seafmedia/ {
                rewrite ^/seafmedia(.*)$ /media$1 break;
                root /opt/seafile/seafile-server-latest/seahub;
            }
            location /seafdav/ {
                proxy_pass         http://127.0.0.1:8080;
                proxy_set_header   Host $host;
                proxy_set_header   X-Real-IP $remote_addr;
                proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header   X-Forwarded-Host $server_name;
                proxy_set_header   X-Forwarded-Proto $scheme;
                proxy_read_timeout  1200s;
                client_max_body_size 0;
                error_log       /var/log/nginx/seafdav.error.log;
            }
```
