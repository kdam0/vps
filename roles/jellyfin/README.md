# Ansible - Jellyfin

## Example Playbook
```
# Direct Access: http://art-jr:8096/
# Domain Access: http://kumardamani.xyz/media
# Create login from GUI.
# While doing initial setup set the base url to '/media' in Networking settings.
- hosts: compute
  gather_facts: false
  tags: 'jellyfin'
  become: true
  roles:
    - role: jellyfin
      vars:
        jellyfin_domain: 'kumardamani.xyz'
```

## Nginx Conf
```
            location /media/ {
                 proxy_pass         http://127.0.0.1:8096/media/;
                 proxy_pass_request_headers on;
                 proxy_set_header Host $host;
                 proxy_set_header X-Real-IP $remote_addr;
                 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                 proxy_set_header X-Forwarded-Proto $scheme;
                 proxy_set_header X-Forwarded-Host $http_host;
                 proxy_set_header Upgrade $http_upgrade;
                 proxy_set_header Connection $http_connection;
                 # Disable buffering when the nginx proxy gets very resource heavy upon streaming
                 proxy_buffering off;
            }
```
