# Run with Docker
```bash
echo "YOURVAULTPASS" > vaultid
docker build . -t ansible
docker run --rm -it -v $(pwd):/home/ansible/vps -v ~/.ssh:/home/ansible/.ssh:ro --user ansible ansible bash
```
